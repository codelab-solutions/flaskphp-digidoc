<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   DigiDoc container implementation: ASICE
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class AsiceContainer extends DigiDocContainerInterface
	{


		/**
		 *   Tmp path
		 *   @var string
		 *   @access protected
		 */

		protected $path = null;


		/**
		 *
		 *   Open/parse the container
		 *   ------------------------
		 *   @access public
		 *   @var string $filename Filename
		 *   @var string $originalFilename Original filename
		 *   @var bool $readMetaDataOnly Read only metadata
		 *   @throws DigiDocException
		 *   @return DigiDocContainerInterface
		 *
		 */

		public function parseContainer( string $filename, string $originalFilename=null, bool $readMetaDataOnly=false )
		{
			// Check for necessary functionality
			if (!class_exists('\\ZipArchive')) throw new DigiDocException('Zip extension not found.');
			if (!class_exists('\\DomDocument')) throw new DigiDocException('DOM extension not found.');
			if (!function_exists('openssl_x509_parse')) throw new DigiDocException('OpenSSL extension not found.');

			// Check and create temp path
			$this->path=Flask()->Config->getTmpPath().'/flaskphp-digidoc-'.uniqid();
			mkdir($this->path);
			if (!is_writable($this->path))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Failed to create temp directory: '.$this->path);
				throw new DigiDocException('Failed to create temp directory.');
			}

			// Try to unpack
			$Zip=new \ZipArchive();
			if ($Zip->open($filename)===true)
			{
				$Zip->extractTo($this->path);
				$Zip->close();
			}
			else
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]] -- Error unpacking ZIP.');
				throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]]');
			}

			// Check for manifest
			if (!is_readable($this->path.'/META-INF/manifest.xml'))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]] -- Could not find META-INF/manifest.xml in '.$this->path);
				throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]]');
			}

			// Parse manifest
			try
			{
				$Manifest=new \DOMDocument();
				$Manifest->load($this->path.'/META-INF/manifest.xml');
				$ns='urn:oasis:names:tc:opendocument:xmlns:manifest:1.0';
				$fileList=$Manifest->documentElement->getElementsByTagNameNS($ns,'file-entry');
				$fileIdx=0;
				foreach ($fileList as $file)
				{
					$fileFullPath=$file->getAttributeNS($ns,'full-path');
					$fileContentType=$file->getAttributeNS($ns,'media-type');
					if ($fileFullPath!='/')
					{
						$File=new DigiDocFile();
						$File->id=intval($fileIdx);
						$File->filename=$fileFullPath;
						$File->contentType=$fileContentType;
						/*
						if ($fileContentType=='application/octet-stream')
						{
							$File->contentType=FlaskPHP\File\File::getMimeType($fileFullPath);
						}
						else
						{
							$File->contentType=$fileContentType;
						}
						*/
						$File->size=filesize($this->path.'/'.$fileFullPath);
						$File->readable=true;
						$File->contentFilename=$this->path.'/'.$fileFullPath;
						if (!$readMetaDataOnly) $File->content=file_get_contents($this->path.'/'.$fileFullPath);
						$this->files[]=$File;
					}
					$fileIdx++;
				}

			}
			catch (\Exception $e)
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]] -- Error parsing manifest.xml: '.$e->getMessage());
				throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]]');
			}

			// Parse signatures
			try
			{
				$sigList=glob($this->path.'/META-INF/signatures*.xml');
				$sigNum=0;
				foreach ($sigList as $sigFile)
				{
					// See if signature is readable
					if (!file_exists($sigFile) || !is_readable($sigFile)) continue;

					// Try to parse
					$SignatureDOM=new \DOMDocument();
					$SignatureDOM->load($sigFile);
					$XPath=new \DOMXpath($SignatureDOM);
					$XPath->registerNamespace('asic','http://uri.etsi.org/02918/v1.2.1#');
					$XPath->registerNamespace('ds','http://www.w3.org/2000/09/xmldsig#');
					$XPath->registerNamespace('xades','http://uri.etsi.org/01903/v1.3.2#');

					// Init
					$Signature=new AsiceSignature();
					$Signature->XML=file_get_contents($sigFile);

					// Get X.509 signature
					$x509signatureList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate',$SignatureDOM);
					foreach ($x509signatureList as $x509signature)
					{
						if(strpos($x509signature->nodeValue, "\n") !== false)
						{
							$nodevalue=trim(strval($x509signature->nodeValue));
						}
						else
						{
							$nodevalue=wordwrap(trim(strval($x509signature->nodeValue)), 64, "\n", true);
						}
						$certificateData=openssl_x509_parse('-----BEGIN CERTIFICATE-----'."\n".$nodevalue."\n".'-----END CERTIFICATE-----');
						if ($certificateData!==false && is_array($certificateData))
						{
							$Signature->certificateData=$certificateData['name'];
							$Signature->country=$certificateData['subject']['C'];
							$Signature->lastName=$certificateData['subject']['SN'];
							$Signature->firstName=$certificateData['subject']['GN'];
							$Signature->idCode=(mb_strpos($certificateData['subject']['serialNumber'],'-')!==false?str_array($certificateData['subject']['serialNumber'],'-',2)[1]:$certificateData['subject']['serialNumber']);
						}
					}

					// See if there's signed time
					$signedTimeList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SigningTime',$SignatureDOM);
					foreach ($signedTimeList as $signedTime)
					{
						$Signature->signedTime=strtotime(strval($signedTime->nodeValue));
					}

					// See if there's a role
					$roleList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignerRole/xades:ClaimedRoles/xades:ClaimedRole',$SignatureDOM);
					foreach ($roleList as $role)
					{
						$Signature->role=strval($role->nodeValue);
					}

					// Address components
					$addressCityList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:City',$SignatureDOM);
					foreach ($addressCityList as $addressCity)
					{
						$Signature->addressCity=strval($addressCity->nodeValue);
					}
					$addressStateList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:StateOrProvince',$SignatureDOM);
					foreach ($addressStateList as $addressState)
					{
						$Signature->addressState=strval($addressState->nodeValue);
					}
					$addressCountryList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:CountryName',$SignatureDOM);
					foreach ($addressCountryList as $addressCountry)
					{
						$Signature->addressCountry=strval($addressCountry->nodeValue);
					}
					$addressZIPList=$XPath->query('/asic:XAdESSignatures/ds:Signature/ds:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:PostalCode',$SignatureDOM);
					foreach ($addressZIPList as $addressZIP)
					{
						$Signature->addressZIP=strval($addressZIP->nodeValue);
					}

					// Add
					$this->signatures[]=$Signature;

					// Continue
					$sigNum++;
				}
			}
			catch (\Exception $e)
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]] -- Error parsing signature #'.intval($sigNum).': '.$e->getMessage());
				throw new DigiDocException('[[ FLASK.DigiDoc.Error.Bdoc.ErrorReadingFile ]]');
			}

			// Return self for chaining
			return $this;
		}


		/**
		 *
		 *   Create a new container
		 *   ----------------------
		 *   @access public
		 *   @static
		 *   @throws DigiDocException
		 *   @return AsiceContainer
		 *
		 */

		public static function createContainer()
		{
			// Check for necessary functionality
			if (!class_exists('\\ZipArchive')) throw new DigiDocException('Zip extension not found.');
			if (!class_exists('\\DomDocument')) throw new DigiDocException('DOM extension not found.');
			if (!function_exists('openssl_x509_parse')) throw new DigiDocException('OpenSSL extension not found.');

			// Create
			$AsiceContainer=new AsiceContainer();

			// Check and create temp path
			$AsiceContainer->path=Flask()->Config->getTmpPath().'/flaskphp-digidoc-'.uniqid();
			mkdir($AsiceContainer->path);
			if (!is_writable($AsiceContainer->path))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Failed to create temp directory: '.$AsiceContainer->path);
				throw new DigiDocException('Failed to create temp directory.');
			}

			// Write mimetype file
			file_put_contents($AsiceContainer->path.'/mimetype','application/vnd.etsi.asic-e+zip');

			// Create META-INF folder
			mkdir($AsiceContainer->path.'/META-INF');

			// Write manifest
			$AsiceContainer->writeManifest();

			// Return container
			return $AsiceContainer;
		}


		/**
		 *
		 *   Add a file to container
		 *   -----------------------
		 *   @access public
		 *   @param string $sourceFile Source file
		 *   @param string $fileName File name (if not the same as source file)
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFile( string $sourceFile, string $fileName=null )
		{
			// Initial checks
			if (empty($this->path)) throw new DigiDocException('Temp directory not found.');
			if (!is_writable($this->path))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Temp directory not writable: '.$this->path);
				throw new DigiDocException('Temp directory not writable.');
			}

			// Can't add more files if already signed
			if (sizeof($this->signatures))
			{
				throw new DigiDocException("Cannot add files to a container that's already signed.");
			}

			// Check source file
			if (!file_exists($sourceFile))
			{
				throw new DigiDocException('Source file does not exist.');
			}
			if (!is_readable($sourceFile))
			{
				throw new DigiDocException('Source file not readable.');
			}

			// Detect filename if not passed
			if ($fileName===null)
			{
				$fileName=pathinfo($sourceFile,PATHINFO_FILENAME);
			}

			// Check for duplicate
			if (file_exists($this->path.'/'.$fileName))
			{
				throw new DigiDocException('File already exists in the container.');
			}

			// Write file
			file_put_contents($this->path.'/'.$fileName,file_get_contents($sourceFile));

			// Add file to structure
			$File=new DigiDocFile();
			$File->id=intval(sizeof($this->files));
			$File->filename=$fileName;
			$File->contentType=FlaskPHP\File\File::getMimeType($this->path.'/'.$fileName);
			$File->size=filesize($this->path.'/'.$fileName);
			$File->readable=true;
			$File->contentFilename=$this->path.'/'.$fileName;
			$this->files[]=$File;

			// Write manifest
			$this->writeManifest();
		}


		/**
		 *
		 *   Add a file to container from string
		 *   -----------------------------------
		 *   @access public
		 *   @param string $fileContent File content
		 *   @param string $fileName File name
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFileFromString( string $fileContent, string $fileName )
		{
			// Initial checks
			if (empty($this->path)) throw new DigiDocException('Temp directory not found.');
			if (!is_writable($this->path))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Temp directory not writable: '.$this->path);
				throw new DigiDocException('Temp directory not writable.');
			}

			// Can't add more files if already signed
			if (sizeof($this->signatures))
			{
				throw new DigiDocException("Cannot add files to a container that's already signed.");
			}

			// Check file
			if (file_exists($this->path.'/'.$fileName))
			{
				throw new DigiDocException('File already exists in the container.');
			}

			// Write file
			file_put_contents($this->path.'/'.$fileName,$fileContent);

			// Add file to structure
			$File=new DigiDocFile();
			$File->id=intval(sizeof($this->files));
			$File->filename=$fileName;
			$File->contentType=FlaskPHP\File\File::getMimeType($this->path.'/'.$fileName);
			$File->size=filesize($this->path.'/'.$fileName);
			$File->readable=true;
			$File->contentFilename=$this->path.'/'.$fileName;
			$this->files[]=$File;

			// Write manifest
			$this->writeManifest();
		}


		/**
		 *
		 *   Get DigiDoc container file
		 *   --------------------------
		 *   @access public
		 *   @param bool $getHashcoded Get hashcoded version?
		 *   @throws DigiDocException
		 *   @return string
		 *
		 */

		public function getDigiDoc( bool $getHashcoded=false )
		{
			// Initial checks
			if (empty($this->path)) throw new DigiDocException('Temp directory not found.');
			if (!sizeof($this->files)) throw new DigiDocException('No files in the container.');
			
			// Check outfile
			$zipFileName=$this->path.'.asice';
			if (file_exists($zipFileName)) throw new DigiDocException('A container already exists in the temp location. Previous getDigiDoc() failed?');

			// Create ZIP container
			try
			{
				// Create file
				$Zip=new \ZipArchive();
				$Zip->open($zipFileName,\ZipArchive::CREATE);

				// Add mimetype
				$Zip->addFromString('mimetype','application/vnd.etsi.asic-e+zip');
				$Zip->setCompressionIndex(0, \ZipArchive::CM_STORE);

				// Add manifest
				$Zip->addFromString('META-INF/manifest.xml',$this->getManifest());

				// Add signatures
				foreach ($this->signatures as $sNum => $Signature)
				{
					$Zip->addFromString('META-INF/signatures'.intval($sNum).'.xml',$Signature->getXML());
				}

				// Add files
				if ($getHashcoded)
				{
					$Zip->addFromString('META-INF/hashcodes-sha256.xml',$this->getHashcodes('sha256'));
					$Zip->addFromString('META-INF/hashcodes-sha512.xml',$this->getHashcodes('sha512'));
				}
				else
				{
					foreach ($this->files as $File)
					{
						$Zip->addFile($this->path.'/'.$File->filename,$File->filename);
					}
				}

				// Close & write
				$Zip->close();

				// Delete temp file and return
				$Zip=file_get_contents($zipFileName);
				unlink($zipFileName);
				return $Zip;
			}
			catch (\Exception $e)
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Failed to create ASIC-E container: '.$e->getMessage());
				throw new DigiDocException('Failed to create ASIC-E container.');
			}
		}


		/**
		 *
		 *   Close the container
		 *   -------------------
		 *   @access public
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function closeContainer()
		{
			// Delete temporary content
			if ($this->path)
			{
				$errors='';
				exec_with_cwd('/bin/rm -rf '.$this->path,Flask()->Config->getTmpPath(),$errors);
			}
		}


		/**
		 *
		 *   Get manifest
		 *   ------------
		 *   @access private
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		private function getManifest()
		{
			// Create manifest
			$manifest='<?xml version="1.0" encoding="UTF-8" standalone="no" ?>'."\n";
			$manifest.='<manifest:manifest xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">'."\n";

				// Root entry
				$manifest.='  <manifest:file-entry manifest:full-path="/" manifest:media-type="application/vnd.etsi.asic-e+zip"/>'."\n";

				// Write files
				foreach ($this->files as $File)
				{
					$manifest.='  <manifest:file-entry manifest:full-path="'.htmlspecialchars($File->filename).'" manifest:media-type="'.htmlspecialchars($File->contentType).'"/>'."\n";
				}

			// Close and return
			$manifest.='</manifest:manifest>'."\n";
			return $manifest;
		}


		/**
		 *
		 *   Write manifest
		 *   --------------
		 *   @access private
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		private function writeManifest()
		{
			// Checks
			if (empty($this->path)) throw new DigiDocException('Temp directory not found.');
			if (!is_writable($this->path))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Failed to create temp directory: '.$this->path);
				throw new DigiDocException('Failed to create temp directory.');
			}
			if (!is_writable($this->path.'/META-INF'))
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('Temp directory not writable: '.$this->path.'/META-INF');
				throw new DigiDocException('Temp directory not writable.');
			}

			// Create manifest
			file_put_contents($this->path.'/META-INF/manifest.xml',$this->getManifest());
		}


		/**
		 *
		 *   Get hashcodes XML
		 *   -----------------
		 *   @access private
		 *   @param string $hashMethod Hash method
		 *   @throws DigiDocException
		 *   @return string
		 *
		 */

		private function getHashcodes( string $hashMethod )
		{
			// File starts
			$hashCodes='<?xml version="1.0" encoding="utf-8"?>'."\n";
			$hashCodes.='<hashcodes>'."\n";

			// Iterate through files
			foreach ($this->files as $File)
			{
				switch ($hashMethod)
				{
					case 'sha256':
					case 'sha512':
						$hash=hash($hashMethod,file_get_contents($this->path.'/'.$File->filename),true);
						break;
					default:
						throw new DigiDocException('Unknown hashMethod: '.$hashMethod);
				}
				$hashCodes.='  <file-entry full-path="'.htmlspecialchars($File->filename).'" hash="'.htmlspecialchars(base64_encode($hash)).'" size="'.intval($File->size).'" />'."\n";
			}

			// End and return
			$hashCodes.='</hashcodes>'."\n";
			return $hashCodes;
		}


	}


?>