<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   Bdoc signature extension
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class BdocSignature extends DigiDocSignature
	{


		/**
		 *   Certificate data
		 *   @var string
		 *   @access public
		 */

		public $certificateData = null;


		/**
		 *   Country
		 *   @var string
		 *   @access public
		 */

		public $country = null;


		/**
		 *   First name
		 *   @var string
		 *   @access public
		 */

		public $firstName = null;


		/**
		 *   Last name
		 *   @var string
		 *   @access public
		 */

		public $lastName = null;


		/**
		 *   ID code
		 *   @var string
		 *   @access public
		 */

		public $idCode = null;


		/**
		 *   Role
		 *   @var string
		 *   @access public
		 */

		public $role = null;


		/**
		 *   Address: city
		 *   @var string
		 *   @access public
		 */

		public $addressCity = null;


		/**
		 *   Address: state
		 *   @var string
		 *   @access public
		 */

		public $addressState = null;


		/**
		 *   Address: country
		 *   @var string
		 *   @access public
		 */

		public $addressCountry = null;


		/**
		 *   Address: ZIP
		 *   @var string
		 *   @access public
		 */

		public $addressZIP = null;


		/**
		 *   Signed time
		 *   @var int
		 *   @access public
		 */

		public $signedTime = null;


		/**
		 *   Signature XML
		 *   @var string
		 *   @access public
		 */

		public $XML = null;


		/**
		 *
		 *   Get signature XML
		 *   -----------------
		 *   @access public
		 *   @throws DigiDocException
		 *   @return string
		 *
		 */

		public function getXML()
		{
			return $this->XML;
		}


	}


?>