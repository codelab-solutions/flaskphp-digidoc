<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   DigiDoc container implementation: DDOC
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class DdocContainer extends DigiDocContainerInterface
	{


		/**
		 *
		 *   Open/parse the container
		 *   ------------------------
		 *   @access public
		 *   @var string $filename Filename
		 *   @var string $originalFilename Original filename
		 *   @var bool $readMetaDataOnly Read only metadata
		 *   @throws DigiDocException
		 *   @return DigiDocContainerInterface
		 *
		 */

		public function parseContainer( string $filename, string $originalFilename=null, bool $readMetaDataOnly=false )
		{
			// Check
			if (!class_exists('\\DomDocument')) throw new DigiDocException('DOM extension not found.');
			if (!is_readable($filename)) throw new DigiDocException('File '.$filename.' not readable.');

			// Load container
			try
			{
				// Open and parse
				$Container=new \DOMDocument();
				$Container->load($filename);

				// Check container
				if ($Container->childNodes[0]->nodeName!='SignedDoc') throw new DigiDocException('Root SignedDoc element not found.');
				if ($Container->childNodes[0]->getAttribute('format')!='DIGIDOC-XML') throw new DigiDocException('Format not DIGIDOC-XML.');

				// Initialize XPath
				$xmlns=$Container->documentElement->namespaceURI;
				$XPath=new \DOMXpath($Container);
				$XPath->registerNamespace('doc',$xmlns);
				$XPath->registerNamespace('signature','http://www.w3.org/2000/09/xmldsig#');
				$XPath->registerNamespace('xades','http://uri.etsi.org/01903/v1.1.1#');

				// Files
				foreach ($Container->getElementsByTagName('DataFile') as $DataFile)
				{
					$File=new DigiDocFile();
					$File->id=strval($DataFile->getAttribute('Id'));
					$File->filename=strval($DataFile->getAttribute('Filename'));
					$File->contentType=strval($DataFile->getAttribute('MimeType'));
					$File->size=intval($DataFile->getAttribute('Size'));
					$File->readable=true;
					if (!$readMetaDataOnly)
					{
						switch (strval($DataFile->getAttribute('ContentType')))
						{
							case 'EMBEDDED_BASE64':
								$File->content=base64_decode($DataFile->nodeValue);
								break;
							default:
								throw new DigiDocException('Unknown DataFile ContentType: '.strval($DataFile->getAttribute('ContentType')));
						}
					}
					$this->files[]=$File;
				}

				// Signatures
				$signatureList=$XPath->query('/doc:SignedDoc/signature:Signature');
				foreach ($signatureList as $Signature)
				{
					$Sig=new DdocSignature();
					$Sig->id=strval($Signature->getAttribute('Id'));

					// Get X.509 signature
					$x509signatureList=$XPath->query('./signature:KeyInfo/signature:X509Data/signature:X509Certificate',$Signature);
					foreach ($x509signatureList as $x509signature)
					{
						if(strpos($x509signature->nodeValue, "\n") !== false)
						{
							$nodevalue=trim(strval($x509signature->nodeValue));
						}
						else
						{
							$nodevalue=wordwrap(trim(strval($x509signature->nodeValue)), 64, "\n", true);
						}
						$certificateData=openssl_x509_parse('-----BEGIN CERTIFICATE-----'."\n".$nodevalue."\n".'-----END CERTIFICATE-----');
						if ($certificateData!==false && is_array($certificateData))
						{
							$Sig->certificateData=$certificateData['name'];
							$Sig->country=$certificateData['subject']['C'];
							$Sig->lastName=$certificateData['subject']['SN'];
							$Sig->firstName=$certificateData['subject']['GN'];
							$Sig->idCode=(mb_strpos($certificateData['subject']['serialNumber'],'-')!==false?str_array($certificateData['subject']['serialNumber'],'-',2)[1]:$certificateData['subject']['serialNumber']);
						}
					}

					// See if there's signed time
					$signedTimeList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SigningTime',$Signature);
					foreach ($signedTimeList as $signedTime)
					{
						$Sig->signedTime=strtotime(strval($signedTime->nodeValue));
					}

					// See if there's a role
					$roleList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignerRole/xades:ClaimedRoles/xades:ClaimedRole',$Signature);
					foreach ($roleList as $role)
					{
						$Sig->role=strval($role->nodeValue);
					}

					// Address components
					$addressCityList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:City',$Signature);
					foreach ($addressCityList as $addressCity)
					{
						$Sig->addressCity=strval($addressCity->nodeValue);
					}
					$addressStateList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:StateOrProvince',$Signature);
					foreach ($addressStateList as $addressState)
					{
						$Sig->addressState=strval($addressState->nodeValue);
					}
					$addressCountryList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:CountryName',$Signature);
					foreach ($addressCountryList as $addressCountry)
					{
						$Sig->addressCountry=strval($addressCountry->nodeValue);
					}
					$addressZIPList=$XPath->query('./signature:Object/xades:QualifyingProperties/xades:SignedProperties/xades:SignedSignatureProperties/xades:SignatureProductionPlace/xades:PostalCode',$Signature);
					foreach ($addressZIPList as $addressZIP)
					{
						$Sig->addressZIP=strval($addressZIP->nodeValue);
					}

					// Add signature
					$this->signatures[]=$Sig;
				}
			}
			catch (\Exception $e)
			{
				if (Flask()->Debug->devEnvironment) throw new DigiDocException('[[ FLASK.DigiDoc.Error.Ddoc.ErrorReadingFile ]] -- Error parsing .ddoc: '.$e->getMessage());
				throw new DigiDocException('[[ FLASK.DigiDoc.Error.Ddoc.ErrorReadingFile ]]');
			}
		}


		/**
		 *
		 *   Create a new container
		 *   ----------------------
		 *   @access public
		 *   @static
		 *   @throws DigiDocException
		 *   @return DdocContainer
		 *
		 */

		public static function createContainer()
		{
			// Ddoc is deprated.
			throw new DigiDocException('The ddoc format is deprecated. Please use bdoc instead.');
		}


		/**
		 *
		 *   Add a file to container
		 *   -----------------------
		 *   @access public
		 *   @param string $sourceFile Source file
		 *   @param string $fileName File name (if not the same as source file)
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFile( string $sourceFile, string $fileName=null )
		{
			// Ddoc is deprated.
			throw new DigiDocException('The ddoc format is deprecated and cannot be modified. Please use bdoc instead.');
		}


		/**
		 *
		 *   Add a file to container from string
		 *   -----------------------------------
		 *   @access public
		 *   @param string $fileContent File content
		 *   @param string $fileName File name
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFileFromString( string $fileContent, string $fileName )
		{
			// Ddoc is deprated.
			throw new DigiDocException('The ddoc format is deprecated and cannot be modified. Please use bdoc instead.');
		}


		/**
		 *
		 *   Get DigiDoc container file
		 *   --------------------------
		 *   @access public
		 *   @param bool $getHashcoded Get hashcoded version?
		 *   @throws DigiDocException
		 *   @return string
		 *
		 */

		public function getDigiDoc( bool $getHashcoded=false )
		{
			// Ddoc is deprated.
			throw new DigiDocException('The ddoc format is deprecated. Please use bdoc instead.');
		}


		/**
		 *
		 *   Close the container
		 *   -------------------
		 *   @access public
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function closeContainer()
		{
			// Nothing needs to be done here.
		}



	}


?>