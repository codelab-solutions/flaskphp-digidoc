<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   DigiDoc container wrapper
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class DigiDocContainer
	{


		/**
		 *
		 *   Open DigiDoc container
		 *   ----------------------
		 *   @access public
		 *   @static
		 *   @var string $filename
		 *   @var string $fileType File type
		 *   @var string $originalFilename Original file name
		 *   @var bool $readMetaDataOnly Read only metadata
		 *   @throws DigiDocException
		 *   @throws DigiDocLockException
		 *   @return DigiDocContainerInterface
		 *
		 */

		public static function openContainer( $filename, $fileType=null, $originalFilename=null, bool $readMetaDataOnly=false )
		{
			// Load locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');

			// Check
			if (!is_readable($filename)) throw new DigiDocException('[[ FLASK.DigiDoc.Error.FileNotReadable ]]');

			// Detect type
			if (!empty($fileType))
			{
				$fileType=mb_strtolower($fileType);
			}
			else
			{
				$pathInfo=pathinfo(oneof($originalFilename,$filename));
				$fileType=mb_strtolower($pathInfo['extension']);
			}

			// Init
			switch ($fileType)
			{
				case 'ddoc':
					$Container=new DdocContainer();
					break;
				case 'bdoc':
					$Container=new BdocContainer();
					break;
				case 'asice':
					$Container=new AsiceContainer();
					break;
				case 'cdoc':
					$Container=new CdocContainer();
					break;
				default:
					throw new DigiDocException('[[ FLASK.DigiDoc.Error.UnknownFileType ]]');
			}


			// Parse file
			$Container->parseContainer($filename,$originalFilename,$readMetaDataOnly);

			// Return container instance
			return $Container;
		}


	}


?>