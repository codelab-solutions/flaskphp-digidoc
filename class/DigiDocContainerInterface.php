<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   DigiDoc container implementation interface
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class DigiDocContainerInterface
	{


		/**
		 *   Files
		 *   @var array
		 *   @access public
		 */

		public $files = array();


		/**
		 *   Signatures
		 *   @var array
		 *   @access public
		 */

		public $signatures = array();


		/**
		 *   Keys
		 *   @var array
		 *   @access public
		 */

		public $keys = array();


		/**
		 *   File info
		 *   @var array
		 *   @access public
		 */

		public $info = array();


		/**
		 *
		 *   The constructor
		 *   ---------------
		 *   @access public
		 *   @return DigiDocContainerInterface
		 *
		 */

		public function __construct()
		{
			// Load locale
			Flask()->Locale->addLocalePath(__DIR__.'/../locale');
		}


		/**
		 *
		 *   Open/parse the container
		 *   ------------------------
		 *   @access public
		 *   @var string $filename Filename
		 *   @var string $originalFilename Original filename
		 *   @var bool $readMetaDataOnly Read only metadata
		 *   @throws DigiDocException
		 *   @return DigiDocContainerInterface
		 *
		 */

		public function parseContainer( string $filename, string $originalFilename=null, bool $readMetaDataOnly=false )
		{
			// This needs to be implemented in the implementation class.
			throw new DigiDocException('Function parseContainer() not implemented.');
		}


		/**
		 *
		 *   Create a new container
		 *   ----------------------
		 *   @access public
		 *   @static
		 *   @throws DigiDocException
		 *   @return BdocContainer
		 *
		 */

		public static function createContainer()
		{
			// This needs to be implemented in the implementation class.
			throw new DigiDocException('Function createContainer() not implemented.');
		}


		/**
		 *
		 *   Add a file to container
		 *   -----------------------
		 *   @access public
		 *   @param string $sourceFile Source file
		 *   @param string $fileName File name (if not the same as source file)
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFile( string $sourceFile, string $fileName=null )
		{
			// This needs to be implemented in the implementation class.
			throw new DigiDocException('Function addFile() not implemented.');
		}


		/**
		 *
		 *   Add a file to container from string
		 *   -----------------------------------
		 *   @access public
		 *   @param string $fileContent File content
		 *   @param string $fileName File name
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function addFileFromString( string $fileContent, string $fileName )
		{
			// This needs to be implemented in the implementation class.
			throw new DigiDocException('Function addFileFromString() not implemented.');
		}


		/**
		 *
		 *   Get DigiDoc container file
		 *   --------------------------
		 *   @access public
		 *   @param bool $getHashcoded Get hashcoded version?
		 *   @throws DigiDocException
		 *   @return string
		 *
		 */

		public function getDigiDoc( bool $getHashcoded=false )
		{
			// This needs to be implemented in the implementation class.
			throw new DigiDocException('Function getDigiDoc() not implemented.');
		}


		/**
		 *
		 *   Close the container
		 *   ------------------------
		 *   @access public
		 *   @throws DigiDocException
		 *   @return void
		 *
		 */

		public function closeContainer()
		{
			// This can be implemented in the subclass if necessary.
		}


	}


?>