<?php


	/**
	 *
	 *   FlaskPHP-DigiDoc
	 *   ----------------
	 *   DigiDoc exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\DigiDoc;
	use Codelab\FlaskPHP;


	class DigiDocException extends FlaskPHP\Exception\Exception
	{
	}


?>